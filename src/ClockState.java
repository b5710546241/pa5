/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.12
 * 
 */
public abstract class ClockState {

	public void enterState(){
		
	}
	
	public void leaveState(){
		
	}
	
	public void setSetButton(){
		
	}

	public void setMinusButton(){

	}

	public void setPlusButton(){

	}
	
}
