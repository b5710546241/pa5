/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.12
 * 
 */
public class DisplayAlarm extends ClockState{

	private ClockGUI clockUI;

	public DisplayAlarm(ClockGUI clock) {
		this.clockUI = clock;
	}

	public void enterState(){
		clockUI.getTimer().stop();
		clockUI.setCurrentTime(clockUI.getClock().getAlarmDate());
	}

	public void leaveState(){
		
	}

	public void setSetButton(){
		
	}

	public void setMinusButton(){

	}

	public void setPlusButton(){
		clockUI.getClock().setState(new DisplayTime(clockUI));
	}
}
