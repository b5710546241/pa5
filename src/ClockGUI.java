/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Date;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.26
 * 
 */
public class ClockGUI extends JFrame {

	public static final int DELAY = 500;
	Clock clock;

	JPanel paneOnAndOff;
	JLabel labelOff;
	
	ImageIcon[] arrImages = new ImageIcon[10];
	ImageIcon[] arrChangingImages = new ImageIcon[10];
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	JLabel label3 = new JLabel();
	JLabel label4 = new JLabel();
	JLabel label5 = new JLabel();
	JLabel label6 = new JLabel();

	javax.swing.Timer timer;
	
	private Clip clip;

	public ClockGUI(Clock clock) {
		this.clock = clock;
		initComponents();
	}

	public void initComponents(){
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		JPanel pane1 = new JPanel();
		pane1.setLayout(new FlowLayout());
		pane1.setBackground(Color.BLACK);
		JPanel pane2 = new JPanel();
		pane2.setLayout(new FlowLayout());
		pane2.setBackground(Color.BLACK);
		
		paneOnAndOff = new JPanel();
		paneOnAndOff.setBackground(Color.BLACK);
		labelOff = new JLabel("OFF");
		labelOff.setForeground(Color.LIGHT_GRAY);
		paneOnAndOff.add(labelOff);

		try {
			clip = AudioSystem.getClip();
			try {
				clip.open(AudioSystem.getAudioInputStream( new BufferedInputStream( getClass().getResourceAsStream("sounds/alarm_beep.wav") ) ) );
			} catch (IOException e) {
				e.printStackTrace();
			} catch (UnsupportedAudioFileException e) {
				e.printStackTrace();
			}
		} catch (LineUnavailableException e) {
				e.printStackTrace();
		}
		 
		for(int i=0; i<10; i++){
			ImageIcon images = new ImageIcon(this.getClass().getResource("images/"+i+".png"));
			arrImages[i] = images;
		}
		
		JLabel colon1 = new JLabel(" : ");
		colon1.setForeground(new Color(255, 198 ,16));
		JLabel colon2 = new JLabel(" : ");
		colon2.setForeground(new Color(255, 198 ,16));
		pane1.add(label1);
		pane1.add(label2);
		pane1.add(colon1);
		pane1.add(label3);
		pane1.add(label4);
		pane1.add(colon2);
		pane1.add(label5);
		pane1.add(label6);
		
		for(int i=0; i<10; i++){
			ImageIcon changingImages = new ImageIcon(this.getClass().getResource("changingimages/"+i+".png"));
			arrChangingImages[i] = changingImages;
		}
		
		JButton setButton = new JButton("SET");
		ActionListener setButtonListener = new setButtonListener();
		setButton.addActionListener(setButtonListener);

		JButton minusButton = new JButton("-");
		ActionListener minusButtonListener = new minusButtonListener();
		minusButton.addActionListener(minusButtonListener);

		JButton plusButton = new JButton("+");
		ActionListener plusButtonListener = new plusButtonListener();
		plusButton.addActionListener(plusButtonListener);

		pane2.add(setButton);
		pane2.add(minusButton);
		pane2.add(plusButton);

		pane.add(paneOnAndOff);
		pane.add(pane1);
		pane.add(pane2);
		this.add(pane);
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		timer = new javax.swing.Timer(DELAY, task);
		timer.start();

	}

	public void setCurrentTime(Date date){
		label1.setIcon( arrImages[date.getHours()/10] );
		label2.setIcon( arrImages[date.getHours()%10] );
		label3.setIcon( arrImages[date.getMinutes()/10] );
		label4.setIcon( arrImages[date.getMinutes()%10] );
		label5.setIcon( arrImages[date.getSeconds()/10] );
		label6.setIcon( arrImages[date.getSeconds()%10] );
	}
	
	public javax.swing.Timer getTimer() {
		return timer;
	}
	
	public Clock getClock(){
		return clock;
	}

	public Clip getClip(){
		return clip;
	}
	
	ActionListener task = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			clock.updateTime();
			setCurrentTime(clock.getDate());
		}
	};

	class setButtonListener implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			clock.getState().setSetButton();
			
		}
	}

	class minusButtonListener implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			clock.getState().setMinusButton();
		}
	}

	class plusButtonListener implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			clock.getState().setPlusButton();
		}
	}

}
