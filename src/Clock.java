/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import java.util.Date;

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.23
 * 
 */
public class Clock {

	private Date date = new Date();
	ClockState state;
	ClockGUI clockUI = new ClockGUI(this);
	private Date alarmDate = new Date();
	
	public Clock(){
		this.state = new DisplayTime(clockUI);
	}
	
	public void setState(ClockState newState){
		if(newState.getClass() != state.getClass()){ 
			state.leaveState();
			newState.enterState();
		}
		this.state = newState;
	}
	
	public ClockState getState(){
		return this.state;
	}
	
	public void updateTime(){
		date.setTime(System.currentTimeMillis());
		this.setAlarmRinging();
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setAlarmDate(Date date){
		this.alarmDate.setTime(date.getTime());
	}
	
	public Date getAlarmDate(){
		return this.alarmDate;
	}
	
	public void setAlarmRinging(){
		if(this.getAlarmDate().getHours() == this.getDate().getHours() &&
		   this.getAlarmDate().getMinutes() == this.getDate().getMinutes() &&
		   this.getAlarmDate().getSeconds() == this.getDate().getSeconds() &&
		   clockUI.labelOff.getText().equals("ON")){
			this.setState(new AlarmRinging(clockUI)); 
		}
	}
	
}
