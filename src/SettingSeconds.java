/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import java.util.Date;

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.26
 * 
 */
public class SettingSeconds extends ClockState {

	private ClockGUI clockUI;
	private Date alarmDate;

	public SettingSeconds(ClockGUI clock){
		this.clockUI = clock;
	}

	public void enterState(){
		setImagesIcon();
	}

	public void leaveState(){
		clockUI.setCurrentTime(clockUI.getClock().getDate());
		alarmDate = clockUI.getClock().getDate();
		clockUI.getClock().setAlarmDate(alarmDate);
		clockUI.labelOff.setText("ON");
	}

	public void setSetButton(){
		clockUI.getClock().setState(new DisplayTime(clockUI));
	}

	public void setMinusButton(){
		clockUI.getClock().getDate().setSeconds(clockUI.getClock().getDate().getSeconds()-1);
		if(clockUI.getClock().getDate().getSeconds() == 59)
			clockUI.getClock().getDate().setMinutes(clockUI.getClock().getDate().getMinutes()+1);
		clockUI.setCurrentTime(clockUI.getClock().getDate());
		setImagesIcon();
	}

	public void setPlusButton(){
		clockUI.getClock().getDate().setSeconds(clockUI.getClock().getDate().getSeconds()+1);
		if(clockUI.getClock().getDate().getSeconds() == 0)
			clockUI.getClock().getDate().setMinutes(clockUI.getClock().getDate().getMinutes()-1);
		clockUI.setCurrentTime(clockUI.getClock().getDate());
		setImagesIcon();
	}
	
	public void setImagesIcon(){
		clockUI.label5.setIcon( clockUI.arrChangingImages[clockUI.getClock().getDate().getSeconds()/10]);
		clockUI.label6.setIcon( clockUI.arrChangingImages[clockUI.getClock().getDate().getSeconds()%10]);
	}

}
