/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.12
 * 
 */
public class DisplayTime extends ClockState {

	private ClockGUI clockUI;

	public DisplayTime(ClockGUI clock) {
		this.clockUI = clock;
	}

	public void enterState(){
		clockUI.getTimer().start();
	}

	public void leaveState(){
		
	}

	public void setSetButton(){
		clockUI.getClock().setState(new SettingHours(clockUI));
	}

	public void setMinusButton(){

	}

	public void setPlusButton(){
		clockUI.getClock().setState(new DisplayAlarm(clockUI));
	}

}
