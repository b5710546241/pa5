/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import javax.sound.sampled.Clip;

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.23
 * 
 */
public class AlarmRinging extends ClockState {

	private ClockGUI clockUI;
	
	public AlarmRinging(ClockGUI clock){
		this.clockUI = clock;
	}
	
	public void enterState(){
		clockUI.getClip().start();
		clockUI.getClip().loop(Clip.LOOP_CONTINUOUSLY);
	}

	public void leaveState(){
		
	}
	
	public void setSetButton(){
		clockUI.getClip().stop();
		clockUI.getClock().setState(new DisplayTime(clockUI));
	}

	public void setMinusButton(){
		clockUI.getClip().stop();
		clockUI.getClock().setState(new DisplayTime(clockUI));
	}

	public void setPlusButton(){
		clockUI.getClip().stop();
		clockUI.getClock().setState(new DisplayTime(clockUI));
	}
	
}
