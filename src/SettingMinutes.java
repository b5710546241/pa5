/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.26
 * 
 */
public class SettingMinutes extends ClockState {

	private ClockGUI clockUI;

	public SettingMinutes(ClockGUI clock){
		this.clockUI = clock;
	}

	public void enterState(){
		setImagesIcon();
	}

	public void leaveState(){
		clockUI.setCurrentTime(clockUI.getClock().getDate());
	}

	public void setSetButton(){
		clockUI.getClock().setState(new SettingSeconds(clockUI));
	}

	public void setMinusButton(){
		clockUI.getClock().getDate().setMinutes(clockUI.getClock().getDate().getMinutes()-1);
		if(clockUI.getClock().getDate().getMinutes() == 59)
			clockUI.getClock().getDate().setHours(clockUI.getClock().getDate().getHours()+1);
		clockUI.setCurrentTime(clockUI.getClock().getDate());
		setImagesIcon();
	}

	public void setPlusButton(){
		clockUI.getClock().getDate().setMinutes(clockUI.getClock().getDate().getMinutes()+1);
		if(clockUI.getClock().getDate().getMinutes() == 0)
			clockUI.getClock().getDate().setHours(clockUI.getClock().getDate().getHours()-1);
		clockUI.setCurrentTime(clockUI.getClock().getDate());
		setImagesIcon();
	}
	
	public void setImagesIcon(){
		clockUI.label3.setIcon( clockUI.arrChangingImages[clockUI.getClock().getDate().getMinutes()/10]);
		clockUI.label4.setIcon( clockUI.arrChangingImages[clockUI.getClock().getDate().getMinutes()%10]);
	}

}
