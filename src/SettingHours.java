/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import java.util.Date;

/**
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.04.26
 * 
 */
public class SettingHours extends ClockState {

	private ClockGUI clockUI;
	
	public SettingHours(ClockGUI clock){
		this.clockUI = clock;
	}
	
	public void enterState(){
		clockUI.getTimer().stop();
		setImagesIcon();
	}

	public void leaveState(){
		clockUI.setCurrentTime(clockUI.getClock().getDate());
	}
	
	public void setSetButton(){
		clockUI.getClock().setState(new SettingMinutes(clockUI));
	}

	public void setMinusButton(){
		clockUI.getClock().getDate().setHours(clockUI.getClock().getDate().getHours()-1);
		clockUI.setCurrentTime(clockUI.getClock().getDate());
		setImagesIcon();
	}

	public void setPlusButton(){
		clockUI.getClock().getDate().setHours(clockUI.getClock().getDate().getHours()+1);
		clockUI.setCurrentTime(clockUI.getClock().getDate());
		setImagesIcon();
	}
	
	public void setImagesIcon(){
		clockUI.label1.setIcon( clockUI.arrChangingImages[clockUI.getClock().getDate().getHours()/10]);
		clockUI.label2.setIcon( clockUI.arrChangingImages[clockUI.getClock().getDate().getHours()%10]);
	}
}
